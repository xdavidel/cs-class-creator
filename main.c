#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_CLASS_NAME 512


char* read_line()
{
  char line[MAX_CLASS_NAME];

  if (NULL != fgets(line, sizeof(line), stdin))
  {
    return strdup(line); 
  }

  return NULL;  
}


void removeStringTrailingNewline(char *str) {
  if (str == NULL) return;
  
  int length = strlen(str);

  if (str[length-1] == '\n') str[length-1]  = '\0';
}


int main()
{
  char *class_name;
  char filename[MAX_CLASS_NAME];
  FILE *fp;

  class_name = read_line();
  removeStringTrailingNewline(class_name);

  if (class_name)
  {
    sprintf(filename, "%s.cs", class_name);

    fp = fopen(filename, "w");

    if (fp)
    {
      fprintf(fp, "class %s : I%s\n{\n\n\n}", class_name, class_name);
      fclose(fp);
    }

    sprintf(filename, "I%s.cs", class_name);

    fp = fopen(filename, "w");

    if (fp)
    {
      fprintf(fp, "interface I%s\n{\n\n\n}", class_name, class_name);
      fclose(fp);
    }

    free(class_name);
  }

  return 0;
}